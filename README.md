# Тестовое задание для стажеров PHP

## Назначение

Тестовое задание для стажеров PHP, которое позволит с помощью практики освоить базовые знания необходимые для поступления на стажировку PHP в компанию [BankiRu](https://www.banki.ru/).

Предлагается решить 4 задачи. 

## Базовые требования
Предусловие:
- Знание основных конструкций PHP и ООП - [документация](https://www.php.net/manual/ru/index.php)
- Разработку необходимо вести на Операционной системе Linux или Mac OS
- Для разработки использовать IDE PHPStorm (можно использовать временный ключ или использовать доступные старые версии)
- Установить git, docker и docker-compose на локальную машину
- Для реализации первых 3 задач используем документацию [Symfony Console Component](https://symfony.com/doc/current/components/console.html)
Запуск проекта:
```shell
docker-compose up -d
```

## Задача 1. Команда для вывода списка персонажей рик и морти
Реализовать консольную команду в `src/Command/GetNamesCommand.php`
```shell
bin/console get_names 1
```
Где 1 это номер эпизода
Ожидаемый результат:
```
Список персонажей 1 эпизода Рика и Морти:
- 1) Rick Sanchez
- 2) ...
```
Есть апи рика и морти https://rickandmortyapi.com/documentation/#get-a-single-episode
Есть symfony console component https://symfony.com/doc/current/components/console.html
Требуется написать консольную команду которая выводит имена персонажей из эпизода по его номеру

## Задача 2. Команда с выводом списка файлов
Реализовать консольную команду в `src/Command/ShowFilesCommand.php`
```shell
bin/console show_files
```
Дан многомерный массив с бесконечной вложенностью
```
$a = [['file1.txt', 'dir1' => ['file2.txt', 'dir2' => ['file3.txt'], 'dir4' => []]]];
```
Требуется вывести полные пути до конечных файлов, если в папке нет файлов не выводим
Ожидаемый результат:
```
1) file1.txt
2) dir1/file2.txt
3) dir1/dir2/file3.txt
```

## Задание 3. Команда для имитации боя
Реализовать консольную команду в `src/Command/ShowFilesCommand.php`
```shell
bin/console fight
```

Написать скрипт fight.php который имитирует бой двух бойцов
   Оба бойца представлены в виде обьекта Champion ($name, $attackMin, $attackMax, $health)
   Каждый раунд они наносят друг другу урон, когда здоровье одного из бойцов упадет ниже 0 объявляется победитель
Пример вывода:
```
Поприветствуем бойцов:
  Боец 1: Петя Осень 100 здоровья, 10-100 атака
  Боец 2: Миша Зима 100 здоровья, 10-100 атака
---
Раунд 1.
   Петя наносит 35 урона
   Миша наносит 30 урона
   Итог: Миша (65 здоровья) / Петя (70 здоровья)
---
Раунд 2.
   Петя наносит 70 урона
   Миша наносит 30 урона
   Итог: Миша (-5 здоровья) / Петя (40 здоровья)
---
Петя побеждает в этом бою!
```
Если ничья выводим "В этом бою ничья"

## Задача 4. Написание api сервиса
Практическая часть подразумевает создание апи сервиса который реализует CRUD(создание/чтение/редактирование/удаление) операции над двумя сущностями пользователь(user) и питомец(pet).
Авторизацию реализовывать не требуется.

Задание состоит из 2 частей:
1. Требуется реализовать api сервис, который реализует OpenApi схему указанную в файле [openapi.json](public/openapi/openapi.json)  
2. Требуется реализовать api тесты для созданных api методов (пример тестов есть в [ExampleApiCest.php](tests/api/Example/ExampleApiCest.php))
- Проверяем 200 ответ
- Проверяем что вернулся json
- Проверяем наличие полей и формат их значений

## Запуск проекта
Запускаем docker 
```shell
make up
```

Устанавливаем зависимости composer
```shell
make composer
```

Выполняем миграции
```shell
make migrate
```

OpenApi схема методов доступна по ссылке http://localhost:8080/
![image](./public/openapi/img.png)

Для примера в проекте реализован api метод http://localhost:8080/api/v1/example/ который возвращает ответ

```json
{
  "message": "hello world"
}
```

## Полезные команды
Запуск api тестов:  
```shell
make tests-api
```

Исправление ошибок форматирования:  
```shell
make csfix
```

Обнаружение ошибок с помощью статического анализатора phpstan:  
```shell
make phpstan
```

Создание автоматических миграций:
```shell
make diff
```

Применение миграций к базе:
```shell
make migrate
```

## Рекомендуется изучить теорию

- HTTP (https://en.wikipedia.org/wiki/HTTP)
- GIT (https://rogerdudler.github.io/git-guide/)
- SQL (https://www.w3schools.com/sql/)
- phpstan (https://github.com/phpstan/phpstan)
- csfixer (https://github.com/PHP-CS-Fixer/PHP-CS-Fixer)
- codeception (https://codeception.com/)
- OpenApi (https://github.com/OAI/OpenAPI-Specification)
- Makefile (https://makefiletutorial.com/)
- bash (https://www.freecodecamp.org/news/bash-scripting-tutorial-linux-shell-script-and-command-line-for-beginners/)
- docker/docker-compose (https://docs.docker.com/)
- Symfony (https://symfony.com/)



