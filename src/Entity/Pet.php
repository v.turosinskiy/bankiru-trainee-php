<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\PetRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PetRepository::class)]
#[ORM\Table(name: 'pet')]
class Pet
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    /** @phpstan-ignore-next-line */
    private int $id;

    #[ORM\Column(unique: true, length: 255)]
    public string $name;

    public function getId(): int
    {
        return $this->id;
    }
}
