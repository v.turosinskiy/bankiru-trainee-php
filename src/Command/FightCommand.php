<?php

declare(strict_types=1);

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'fight')]
class FightCommand extends Command
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $text = 'Поприветствуем бойцов:
  Боец 1: Петя Осень 100 здоровья, 10-100 атака
  Боец 2: Миша Зима 100 здоровья, 10-100 атака
---
Раунд 1.
   Петя наносит 35 урона
   Миша наносит 30 урона
   Итог: Миша (65 здоровья) / Петя (70 здоровья)
---
Раунд 2.
   Петя наносит 70 урона
   Миша наносит 30 урона
   Итог: Миша (-5 здоровья) / Петя (40 здоровья)
---
Петя побеждает в этом бою!';
        $output->writeln($text);

        return Command::SUCCESS;
    }
}
