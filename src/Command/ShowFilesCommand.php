<?php

declare(strict_types=1);

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'show_files')]
class ShowFilesCommand extends Command
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $a = [['file1.txt', 'dir1' => ['file2.txt', 'dir2' => ['file3.txt'], 'dir4' => []]]];;
        //...
        $output->writeln('1) file1.txt
2) dir1/file2.txt
3) dir1/dir2/file3.txt');

        return Command::SUCCESS;
    }
}
